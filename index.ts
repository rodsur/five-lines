const TILE_SIZE = 30;
const FPS = 30;
const SLEEP = 1000 / FPS;

enum RawTile {
    AIR,
    FLUX,
    UNBREAKABLE,
    PLAYER,
    STONE, FALLING_STONE,
    BOX, FALLING_BOX,
    KEY1, LOCK1,
    KEY2, LOCK2
}
class Player {
    private x = 1;
    private y = 1;
    
    draw(g: CanvasRenderingContext2D) {
        g.fillStyle = "#ff0000";
        g.fillRect(this.x * TILE_SIZE, this.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }
    moveHorizontal(dx: number) {
        map[this.y][this.x + dx].moveHorizontal(dx, this);
    }
    moveVertical(dy: number) {
        map[this.y + dy][this.x].moveVertical(dy, player);
    }
    move(dx: number, dy: number) {
        this.moveToTile(this.x + dx, this.y + dy);
    }
    pushHorizontal(tile: Tile, dx: number) {
        if (map[this.y][this.x + dx + dx].isAir()
            && !map[this.y + 1][this.x + dx].isAir()) {
            map[this.y][this.x + dx + dx] = tile;
            this.moveToTile(this.x + dx, this.y);
        }
    }
    private moveToTile(newx: number, newy: number) {
        map[this.y][this.x] = new Air();
        map[newy][newx] = new PlayerTile();
        this.x = newx;
        this.y = newy;
    }
}
interface removeStrategy {
    check(tile: Tile): boolean;
}
class RemoveLock1 implements removeStrategy{
    check(tile: Tile) {
        return tile.isLock1();
    }
}
class RemoveLock2 implements removeStrategy{
    check(tile: Tile) {
        return tile.isLock2();
    }
}
class KeyConfiguration {
    constructor(private color: string, private _1: boolean, private removeStrategy: removeStrategy) {}

    is1() {
        return this._1;
    }
    
    removeLock() {
        remove(this.removeStrategy);
    }
    
    setColor(g:CanvasRenderingContext2D) {
        g.fillStyle = this.color;
    }
}
interface FallingState {
    isFalling(): boolean;
    moveHorizontal(tile: Tile, dx: number, player: Player): void;
    drop(tile: Tile, x: number, y: number): void;
}
class Falling implements FallingState {
    isFalling(): boolean {
        return true;
    }
    moveHorizontal(tile: Tile, dx: number, player: Player) {
    }
    
    drop(tile: Tile, x: number, y: number) {
        map[y + 1][x] = tile;
        map[y][x] = new Air();
    }
}

class Resting implements FallingState {
    isFalling(): boolean {
        return false;
    }
    moveHorizontal(tile: Tile, dx: number, player: Player) {
        player.pushHorizontal(tile, dx);
    }
    drop(tile: Tile, x: number, y: number) {}
}
class FallStrategy {
    constructor(private falling: FallingState) {}

    update(tile: Tile, x: number, y: number) {
        this.falling = map[y + 1][x].getBlockOnTopState();
        this.drop(tile, x, y);
        
    }
    private drop(tile: Tile, x: number, y: number) {
        this.falling.drop(tile, x, y);
    }
    
    moveHorizontal(tile: Tile, dx: number) {
        this.falling.moveHorizontal(tile, dx, player);
    }
}
interface Tile {
    isAir(): boolean;
    isLock1(): boolean;
    isLock2(): boolean;
    canFall(): boolean;
    
    draw(g: CanvasRenderingContext2D, x: number, y: number): void;
    moveHorizontal(dx: number, player: Player): void;
    moveVertical(dy: number, player: Player): void;
    update(x: number, y: number): void;
    getBlockOnTopState(): FallingState;
}

class Flux implements Tile {
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return false;
    }

    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        g.fillStyle = "#ccffcc";
        g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }
    moveHorizontal(dx: number, player: Player) {
        player.move(dx, 0);
    }
    
    moveVertical(dy: number, player: Player) {
        player.move(0, dy);
    }
    update(x: number, y: number) {}
    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class Unbreakable implements Tile {
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return false;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        g.fillStyle = "#999999";
            g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }

    moveHorizontal(dx: number) {
    }
    
    moveVertical(dy: number) {
    }
    update(x: number, y: number) {}

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class Stone implements Tile {
    private fallStrategy: FallStrategy;
    constructor(falling: FallingState) {
        this.fallStrategy = new FallStrategy(falling);
    }
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return true;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        g.fillStyle = "#0000cc";
            g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }

    moveHorizontal(dx: number) {
        this.fallStrategy.moveHorizontal(this, dx)
    }

    moveVertical(dy: number) {
    }

    
    update(x: number, y: number) {
        this.fallStrategy.update(this, x, y);
    }

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class Air implements Tile {
    isAir(): boolean {
        return true;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return false;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
    }

    moveHorizontal(dx: number, player: Player) {
        player.move(dx, 0);
    }

    moveVertical(dy: number, player: Player) {
        player.move(0, dy);
    }
    update(x: number, y: number) {}

    getBlockOnTopState(): FallingState {
        return new Falling();
    }
}

class PlayerTile implements Tile {
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return false;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
    }

    moveHorizontal(dx: number) {
    }

    moveVertical(dy: number) {
    }
    update(x: number, y: number) {}

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class Box implements Tile {
    private fallStrategy: FallStrategy;
    constructor(falling: FallingState) {
        this.fallStrategy = new FallStrategy(falling);
    }
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return true;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        g.fillStyle = "#8b4513";
            g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }

    moveHorizontal(dx: number) {
        this.fallStrategy.moveHorizontal(this, dx)
    }

    moveVertical(dy: number) {
    }
    update(x: number, y: number) {
        this.fallStrategy.update(this, x, y);
    }

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class Key implements Tile {
    constructor(private keyConf: KeyConfiguration) {
    }
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return false;
    }
    isLock2(): boolean {
        return false;
    }
    canFall(): boolean {
        return false;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        this.keyConf.setColor(g);
        g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }

    moveHorizontal(dx: number, player: Player) {
        this.keyConf.removeLock();
        player.move(dx, 0);
    }

    moveVertical(dy: number, player: Player) {
        this.keyConf.removeLock();
        player.move(0, dy);
    }
    update(x: number, y: number) {}

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

class LockTile implements Tile {
    constructor(private keyConf: KeyConfiguration) {
    }
    isAir(): boolean {
        return false;
    }
    isLock1(): boolean {
        return this.keyConf.is1();
    }
    isLock2(): boolean {
        return !this.keyConf.is1();
    }
    canFall(): boolean {
        return false;
    }
    
    draw(g: CanvasRenderingContext2D, x: number, y: number) {
        this.keyConf.setColor(g);
        g.fillRect(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }

    moveHorizontal(dx: number) {
    }

    moveVertical(dy: number) {
    }
    update(x: number, y: number) {}

    getBlockOnTopState(): FallingState {
        return new Resting();
    }
}

interface Input2 {
    isRight(): boolean;

    isLeft(): boolean;

    isUp(): boolean;

    isDown(): boolean;

    handle(): void;
}

class Right implements Input2 {
    isRight(): boolean {
        return true;
    }

    isLeft(): boolean {
        return false;
    }

    isUp(): boolean {
        return false;
    }

    isDown(): boolean {
        return false;
    }

    handle() {
        player.moveHorizontal(1);
        
    }
}

class Left implements Input2 {
    isRight(): boolean {
        return false;
    }

    isLeft(): boolean {
        return true;
    }

    isUp(): boolean {
        return false;
    }

    isDown(): boolean {
        return false;
    }

    handle() {
        player.moveHorizontal(-1);
    }
}

class Up implements Input2 {
    isRight(): boolean {
        return false;
    }

    isLeft(): boolean {
        return false;
    }

    isUp(): boolean {
        return true;
    }

    isDown(): boolean {
        return false;
    }

    handle() {
        player.moveVertical(-1);
    }
}

class Down implements Input2 {
    isRight(): boolean {
        return false;
    }

    isLeft(): boolean {
        return false;
    }

    isUp(): boolean {
        return false;
    }

    isDown(): boolean {
        return true;
    }

    handle() {
        player.moveVertical(1);
    }
}

let player = new Player();
let rawMap: RawTile[][] = [
    [2, 2, 2, 2, 2, 2, 2, 2],
    [2, 3, 0, 1, 1, 2, 0, 2],
    [2, 4, 2, 6, 1, 2, 0, 2],
    [2, 8, 4, 1, 1, 2, 0, 2],
    [2, 4, 1, 1, 1, 9, 0, 2],
    [2, 2, 2, 2, 2, 2, 2, 2],
];
let map: Tile[][];

function assertExhausted(x: never): never {
    throw new Error("Unexpected object: " + x);
}

function transformTile(tile: RawTile) {
    switch (tile) {
        case RawTile.AIR:
            return new Air();
        case RawTile.PLAYER:
            return new PlayerTile();
        case RawTile.UNBREAKABLE:
            return new Unbreakable();
        case RawTile.STONE:
            return new Stone(new Resting());
        case RawTile.FALLING_STONE:
            return new Stone(new Falling());
        case RawTile.BOX:
            return new Box(new Resting());
        case RawTile.FALLING_BOX:
            return new Box(new Falling());
        case RawTile.FLUX:
            return new Flux();
        case RawTile.KEY1:
            return new Key(YELLOW_KEY);
        case RawTile.LOCK1:
            return new LockTile(YELLOW_KEY);
        case RawTile.KEY2:
            return new Key(BLUE_KEY);
        case RawTile.LOCK2:
            return new LockTile(BLUE_KEY);
        default:
            assertExhausted(tile);
    }
}

function transformMap() {
    map = new Array(rawMap.length);
    for (let y = 0; y < rawMap.length; y++) {
        map[y] = new Array(rawMap[y].length);
        for (let x = 0; x < rawMap[y].length; x++) {
            map[y][x] = transformTile(rawMap[y][x]);
        }
    }
}

let inputs: Input2[] = [];


function remove(shouldRemove: removeStrategy) {
    for (let y = 0; y < map.length; y++) {
        for (let x = 0; x < map[y].length; x++) {
            if (shouldRemove.check(map[y][x])) {
                map[y][x] = new Air();
            }
        }
    }
}
const YELLOW_KEY = new KeyConfiguration("#ffcc00", true, new RemoveLock1());
const BLUE_KEY = new KeyConfiguration("#00ccff", false, new RemoveLock2());

function updateMap() {
    for (let y = map.length - 1; y >= 0; y--) {
        for (let x = 0; x < map[y].length; x++) {
            map[y][x].update(x, y);
        }
    }
}

function handleInputs() {
    while (inputs.length > 0) {
        let current = inputs.pop();
        current.handle();
    }
}

function update() {
    handleInputs();
    updateMap();
}

function draw() {
    let g = createGraphics();
    drawMap(g);
    player.draw(g);
}

function drawMap(g: CanvasRenderingContext2D) {
    for (let y = 0; y < map.length; y++) {
        for (let x = 0; x < map[y].length; x++) {
            map[y][x].draw(g, x, y);
        }
    }
}

function createGraphics() {
    let canvas = document.getElementById("GameCanvas") as HTMLCanvasElement;
    let g = canvas.getContext("2d");
    g.clearRect(0, 0, canvas.width, canvas.height);
    return g;
}

function gameLoop() {
    let before = Date.now();
    update();
    draw();
    let after = Date.now();
    let frameTime = after - before;
    let sleep = SLEEP - frameTime;
    setTimeout(() => gameLoop(), sleep);
}

window.onload = () => {
    transformMap();
    gameLoop();
}

const LEFT_KEY = "ArrowLeft";
const UP_KEY = "ArrowUp";
const RIGHT_KEY = "ArrowRight";
const DOWN_KEY = "ArrowDown";
window.addEventListener("keydown", e => {
    if (e.key === LEFT_KEY || e.key === "a") inputs.push(new Left());
    else if (e.key === UP_KEY || e.key === "w") inputs.push(new Up());
    else if (e.key === RIGHT_KEY || e.key === "d") inputs.push(new Right());
    else if (e.key === DOWN_KEY || e.key === "s") inputs.push(new Down());
});

